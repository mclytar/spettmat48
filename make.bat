@echo off
@echo Making 'master'...

pdflatex master.tex -c-style-errors -quiet
IF %errorlevel% NEQ 0 GOTO :error
ECHO Phase 1 completed...
pdflatex master.tex -c-style-errors -quiet
IF %errorlevel% NEQ 0 GOTO :error
ECHO [92mDONE![0m

ECHO Making 'slave-scena6'...
pdflatex slave-scena6.tex -c-style-errors -quiet
IF %errorlevel% NEQ 0 GOTO :error
ECHO Phase 1 completed...
pdflatex slave-scena6.tex -c-style-errors -quiet
IF %errorlevel% NEQ 0 GOTO :error
ECHO [92mDONE![0m

GOTO :eof

:error
ECHO.
ECHO [31mAborting.[0m