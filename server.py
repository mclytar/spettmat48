from http.server import BaseHTTPRequestHandler, HTTPServer, SimpleHTTPRequestHandler
from os import path

PATH = path.abspath(".")
PORT = 8000

saddr = ("", PORT)

class Handler(SimpleHTTPRequestHandler):
    pass

Handler.extensions_map = {
    '.css': 'text/css',
    '.html': 'text/html',
    '.js': 'application/x-javascript',
    '.png': 'image/png',
    '.tex': 'text/plain',
    '': 'application/octet-stream',
}

httpd = HTTPServer(saddr, Handler)

print("Starting service from", PATH)

httpd.serve_forever()