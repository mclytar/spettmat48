\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{Generale}{2}{chapter*.2}
\contentsline {section}{Diario di bordo}{2}{section*.3}
\contentsline {subsection}{4 ottobre 2018}{2}{subsection*.4}
\contentsline {subsection}{22 ottobre 2018}{2}{subsection*.5}
\contentsline {section}{Incarichi}{3}{section*.6}
\contentsline {section}{Elenco scene}{5}{section*.7}
\contentsline {chapter}{Organizzazione}{6}{chapter*.8}
\contentsline {section}{Personaggi}{6}{section*.9}
\contentsline {section}{Oggetti di scena}{9}{section*.10}
\contentsline {section}{Costumi}{10}{section*.11}
\contentsline {section}{Musica}{11}{section*.12}
\contentsline {subsection}{Tracce musicali ed effetti sonori}{11}{subsection*.13}
\contentsline {subsection}{Testi delle canzoni}{11}{subsection*.14}
\contentsline {subsubsection}{Il limite che non c'\IeC {\`e}}{11}{subsubsection*.15}
\contentsline {subsubsection}{E}{12}{subsubsection*.16}
\contentsline {subsubsection}{Toto - Algebra}{13}{subsubsection*.17}
\contentsline {subsubsection}{Aria Eulero-Wendy, parte 1}{13}{subsubsection*.18}
\contentsline {subsubsection}{Rap parte 1}{14}{subsubsection*.19}
\contentsline {subsubsection}{Aria Eulero-Wendy, parte 2}{14}{subsubsection*.20}
\contentsline {subsubsection}{Rap, parte 2}{15}{subsubsection*.21}
\contentsline {subsubsection}{Barbiere}{15}{subsubsection*.22}
\contentsline {subsubsection}{Capitan Hooke}{16}{subsubsection*.23}
\contentsline {subsubsection}{Titanic}{17}{subsubsection*.24}
\contentsline {subsubsection}{$6 \times 7 + 2$ gatti}{17}{subsubsection*.25}
\contentsline {section}{Scene}{19}{section*.26}
\contentsline {subsection}{I genitori di Vendy}{19}{subsection*.27}
\contentsline {subsection}{Vendy a casa}{22}{subsection*.28}
\contentsline {subsection}{Uncino, pirati, spugna}{24}{subsection*.29}
\contentsline {subsection}{Bimbi sperduti}{26}{subsection*.30}
\contentsline {subsection}{Giro dell'isola}{29}{subsection*.31}
\contentsline {subsubsection}{PREMESSE}{29}{subsubsection*.32}
\contentsline {subsubsection}{6a1 - L'AULA III}{30}{subsubsection*.33}
\contentsline {subsubsection}{6a2 - PINOCCHIO}{31}{subsubsection*.34}
\contentsline {subsubsection}{6a3 - IL PASSATO}{34}{subsubsection*.35}
\contentsline {subsubsection}{6b1 - ORDA 1}{36}{subsubsection*.36}
\contentsline {subsubsection}{6b3 - X FACTORIALE}{38}{subsubsection*.37}
\contentsline {subsubsection}{6b4 - LA BUROCRAZIA}{39}{subsubsection*.38}
\contentsline {subsubsection}{6c1 - IL PRESENTE}{40}{subsubsection*.39}
\contentsline {subsection}{Giglio Fibrato}{42}{subsection*.40}
\contentsline {subsection}{Gli indiani}{45}{subsection*.41}
\contentsline {subsection}{Trilli da Uncino}{48}{subsection*.42}
\contentsline {subsection}{Rapimento bimbi}{50}{subsection*.43}
\contentsline {subsection}{Morte di Trilli}{52}{subsection*.44}
\contentsline {subsection}{Scontro finale}{53}{subsection*.45}
\contentsline {subsection}{Ritorno a casa}{55}{subsection*.46}
