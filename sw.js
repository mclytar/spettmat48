// Evento install
self.addEventListener('install', event => {
    console.log("[SW] Installed!");

    event.waitUntil(self.skipWaiting())
});

// Evento activate 
self.addEventListener('activate', event => {
    console.log("[SW] Activated!");
    event.waitUntil(self.clients.claim());
});

// Evento message
self.addEventListener('message', event => {
    //console.log("[SW] Received: ", event.data);

    // event.ports[0].postMessage(event.data);

    broadcast(event.data);
});

function send_message(client, message) {
    return new Promise((resolve, reject) => {
        let msg_ch = new MessageChannel();

        msg_ch.port1.onmessage = event => {
            if (event.data.error) reject(event.data.error);
            else resolve(event.data);
        }

        client.postMessage(message, [msg_ch.port2]);
    });
}

// Funzione broadcast
function broadcast(message) {
    self.clients.matchAll().then(clients => {
        clients.forEach(client => {
            send_message(client, message).then(m => {
                //console.log("[SW] Broadcasting: ", message);
            });
        });
    });
}