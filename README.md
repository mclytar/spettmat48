# Natale al Castelnuovo 2018

Copione e gestione dello spettacolo di Natale 2018 al dipartimento di matematica Guido Castelnuovo.

## Informazioni preliminari

Questo file `README.md` è ancora una bozza e sarà migliorato in seguito.  
  
Il file ufficiale è `master.pdf`, in cui risiedono tutti i dettagli riguardo alla recita (copione, oggetti di scena, costumi di scena, etc.). Questo file è una compilazione in LaTeX del file `master.tex` che, a sua volta, richiama i files in `partial/`, dove risiendono dettagli particolari (personaggi, costumi, tracce audio, canzoni, ...), e `scenes/`, in cui sono collocate le scene vere e proprie.  
  
Il software di gestione in tempo reale è stato scritto in HTML e JavaScript, sfruttando la libreria jQuery e lo script in python `server.py` per avviare il server (necessario per caricare i files in LaTeX in modo dinamico).  
  
I files musicali risiedono nella cartella `audio/` ma, per vari motivi, al momento ho preferito non includerli nel repository.

## Autori

Gli autori e tutti coloro che hanno contribuito sono elencati [qui](CONTRIBUTORS.md).

## Crediti

Il template grafico utilizzato in LaTeX è [D&D 5e LaTeX Template](https://github.com/evanbergeron/DND-5e-LaTeX-Template).  
  
Il software per la gestione in tempo reale sfrutta la libreria [jQuery](https://jquery.com/).  

## Licenza

[MIT](LICENSE) (in inglese)

