
let sw = {
    install: function() {
        navigator.serviceWorker.register('./sw.js').then((registration) => {
            console.log("ServiceWorker registered correctly!");

            registration.update();

            navigator.serviceWorker.addEventListener('message', event => {
                if (event.data.target == 'main') sw.onmessage(event.data);
            });
        });
    },
    broadcast: function(message) {
        return new Promise((resolve, reject) => {
            let msg_ch = new MessageChannel();
    
            msg_ch.port1.onmessage = event => {
                if (event.data.error) reject(event.data.error);
                else resolve(event.data);
            }

            navigator.serviceWorker.controller.postMessage(message, [msg_ch.port2]);
        });
    },
    onmessage: data => {
        console.log(data);
    }
};

let screen_tools = {
    show: function () {
        $("#screen-window").fadeIn(200);
    },
    lyrics: {
        show: function () {
            $("#screen-tab-1-head").text("Canzoni");
            $("#screen-tab-2-head").text("");
            $("#screen-tab-2").text("");

            screen_tools.lyrics.status.song = null;
            screen_tools.lyrics.status.row = null;

            $("#screen-tab-1").html("");
            for (let i = 0; i < audio.lyrics.length; i++) {
                $("#screen-tab-1").append("<a href='#' onclick='screen_tools.lyrics.load(" + i + ");'>" + audio.lyrics[i].title + "</a>");
            }
            $("#screen-tab-1").append("<hr />");
            $("#screen-tab-1").append("<span id='screen-tab-1-song-label' class='label'>Attuale</span>");
            $("#screen-tab-1").append("<span id='screen-tab-1-song' class='static'></span>");
            $("#screen-tab-1").append("<span id='screen-tab-1-row' class='static'></span>");
            $("#screen-tab-1").append("<a id='screen-tab-1-song-prev' href='#' onclick='screen_tools.lyrics.prev();'>Precedente</a>");
            $("#screen-tab-1").append("<a id='screen-tab-1-song-next' href='#' onclick='screen_tools.lyrics.next();'>Successiva</a>");

            screen_tools.lyrics.update_nav();
        },
        load: function(index) {
            $("#screen-tab-2-head").text(audio.lyrics[index].title);

            let wrapper = $('<div>');

            for (let i = 0; i < audio.lyrics[index].content.length; i++) {
                if (audio.lyrics[index].content[i] == '') {
                    wrapper.append("<a href='#' onclick='screen_tools.lyrics.print(" + index + ", " + i + ");'><hr /></a>");
                } else {
                    wrapper.append("<a href='#' onclick='screen_tools.lyrics.print(" + index + ", " + i + ");'>" + (i + 1) + ". " + audio.lyrics[index].content[i] + "</a>");
                }
            }

            $("#screen-tab-2").html("");
            $("#screen-tab-2").append(wrapper);

            screen_tools.lyrics.status.song = index;
            screen_tools.lyrics.update_nav();
        },
        print: function(index, row) {
            screen_tools.lyrics.status.song = index;
            screen_tools.lyrics.status.row = row;

            let message = {
                target: 'screen',
                action: 'lyric-set',
                content: audio.lyrics[index].content[row]
            };
            if (screen_tools.settings.flag_next) {
                if (row + 1 < audio.lyrics[index].content.length) {
                    message.next = audio.lyrics[index].content[row + 1];
                }
            }

            sw.broadcast(message);

            screen_tools.lyrics.update_nav();
        },
        prev: function () {
            let song = screen_tools.lyrics.status.song;
            let row = screen_tools.lyrics.status.row;
            if (song !== null && row !== null && row > 0) {
                screen_tools.lyrics.print(song, row - 1);
                // screen_tools.lyrics.status.row--;
            }

            screen_tools.lyrics.update_nav();
        },
        next: function () {
            let song = screen_tools.lyrics.status.song;
            let row = screen_tools.lyrics.status.row;
            if (song !== null && row !== null && (row + 1) < audio.lyrics[song].content.length) {
                screen_tools.lyrics.print(song, row + 1);
                // screen_tools.lyrics.status.row++;
            }

            screen_tools.lyrics.update_nav();
        },
        status: {
            song: null,
            row: null
        },
        update_nav: function () {
            if (screen_tools.lyrics.status.song !== null) {
                $("#screen-tab-1-song").text(audio.lyrics[screen_tools.lyrics.status.song].title)
                
                if (screen_tools.lyrics.status.row !== null) {
                    $("#screen-tab-1-row").text("Riga " + (screen_tools.lyrics.status.row + 1));
                    $("#screen-tab-1-song-prev").removeClass('disabled');
                    $("#screen-tab-1-song-next").removeClass('disabled');
                    $("#screen-tab-1-song-prev").attr('onclick', 'screen_tools.lyrics.prev();');
                    $("#screen-tab-1-song-next").attr('onclick', 'screen_tools.lyrics.next();');
                } else {
                    $("#screen-tab-1-row").html("<i>Nessuna riga selezionata</i>");
                    $("#screen-tab-1-song-prev").addClass('disabled');
                    $("#screen-tab-1-song-next").addClass('disabled');
                    $("#screen-tab-1-song-prev").attr('onclick', '');
                    $("#screen-tab-1-song-next").attr('onclick', '');
                }
            } else {
                $("#screen-tab-1-song").html("<i>Nessuna canzone selezionata</i>");
                $("#screen-tab-1-row").html("<i>Nessuna riga selezionata</i>");
                $("#screen-tab-1-song-prev").addClass('disabled');
                $("#screen-tab-1-song-next").addClass('disabled');
                $("#screen-tab-1-song-prev").attr('onclick', '');
                $("#screen-tab-1-song-next").attr('onclick', '');
            }
        }
    },
    settings: {
        show: function () {
            $("#screen-tab-1-head").text("Impostazioni");
            $("#screen-tab-2-head").text("");

            screen_tools.lyrics.status.song = null;
            screen_tools.lyrics.status.row = null;
            screen_tools.lyrics.update_nav();

            $("#screen-tab-1").text("");
            $("#screen-tab-2").text("");
        },
        flag_next: false
    },
    clear: function () {
        $("#screen-tab-1-head").text("");
        $("#screen-tab-2-head").text("");

        screen_tools.lyrics.status.song = null;
        screen_tools.lyrics.status.row = null;
        screen_tools.lyrics.update_nav();

        $("#screen-tab-1").text("");
        $("#screen-tab-2").text("");

        sw.broadcast({
            target: 'screen',
            action: 'clear'
        });
    },
    custom: {
        show: function () {
            $("#screen-tab-1-head").text("Formato");
            $("#screen-tab-2-head").text("Testo");

            screen_tools.lyrics.status.song = null;
            screen_tools.lyrics.status.row = null;
            screen_tools.lyrics.update_nav();

            $("#screen-tab-1").html("");
            $("#screen-tab-1").append("<a href='#' onclick='screen_tools.custom.print_text()'>Stampa testo</a>");
            $("#screen-tab-1").append("<a href='#' onclick='screen_tools.custom.print_html()'>Stampa html</a>");

            $("#screen-tab-2").html("");
            $("#screen-tab-2").append("<textarea id='custom-text'></textarea>");
        },
        print_text: function () {
            sw.broadcast({
                target: 'screen',
                action: 'text-set',
                content: $("#custom-text").val()
            });
        },
        print_html: function () {
            sw.broadcast({
                target: 'screen',
                action: 'html-set',
                content: $("#custom-text").val()
            });
        }
    },
    hide: function () {
        $("#screen-window").fadeOut(200);
    }
};

let characters = {
    call: (id) => {
        for (i = 0; i < characters.list.length; i++) {
            if (characters.list[i].id == id) return characters.list[i].name;
        }
    },
    list: [],
    load: function (data) {
        let regex = /\\CreateCharacter\{([^\}]+)\}\{([^\}]+)\}\{([^\}]+)\}\{([^\}]+)\}\{([^\}]*)\}\{([^\}]*)\}/g;
    
        let match = regex.exec(data);
    
        while (match != null) {
            $("#loading-bar-slave").attr("name", match[2]);
            characters.list.push({
                id: match[1],
                name: match[2],
                fullname: match[3],
                actor: match[4],
                callnames: [match[5], match[6]]
            });
    
            match = regex.exec(data);
        }
        $("#loading-bar-slave").attr("name", "");
    }.bind(this),
    name: (id, num = 0) => {
        for (i = 0; i < characters.list.length; i++) {
            if (characters.list[i].id == id) return characters.list[i].callnames[num];
        }
    }
};

let audio = {
    list: [],
    lyrics: [],
    current_id: null,
    load: function (data) {
        // Load audio files
        let regex = /\\(CreateMusic|CreateRecordedMusic|CreateSoundEffect)\{([^\}]*)\}\{([^\}]*)\}/g;
        let match = regex.exec(data);
        while (match != null) {
            let track = {
                id: match[2],
                kind: "",
                title: match[3],
                fotime: 1.0
            };

            track.audio = new Audio("./audio/" + track.id + ".mp3");
            track.audio.load();

            console.log(track.audio.error);

            track.error = true;

            if (match[1] == "CreateMusic") track.kind = "live";
            if (match[1] == "CreateRecordedMusic") track.kind = "playback";
            if (match[1] == "CreateSoundEffect") track.kind = "sound";

            audio.list.push(track);

            let id = match[2];

            $.ajax("./audio/" + track.id + ".mp3", {
                async: false,
                cache: false,
                method: 'HEAD',
                success: () => {
                    for (let i = 0; i < audio.list.length; i++) {
                        if (audio.list[i].id == id) audio.list[i].error = false;
                    }
                },
                error: () => {
                    for (let i = 0; i < audio.list.length; i++) {
                        if (audio.list[i].id == id) audio.list[i].error = true;
                    }
                }
            });

            match = regex.exec(data);
        }

        // Load songs
        regex = /%\?song-start([\w\W]*?)%\?song-end/g;
        match = regex.exec(data);
        while(match != null) {
            song = {
                content: []
            };
            // Song title
            regex_title = /\\subsubsection\{([^\}]*)\}/g
            song.title = regex_title.exec(match[1])[1];
            regex_row = /^([^\\\{\s][\W\w]*?|\\mbox\{\})\\\\/gm;
            match_row = regex_row.exec(match[1]);
            while(match_row != null) {
                if (match_row[1] == '\\mbox{}') {
                    song.content.push("");
                } else {
                    song.content.push(match_row[1]);
                }
                match_row = regex_row.exec(match[1]);
            }

            audio.lyrics.push(song);

            match = regex.exec(data);
        }
    },
    title: function (id) {
        for (let i = 0; i < audio.list.length; i++) {
            if (audio.list[i].id == id) return audio.list[i].title;
        }
    },
    kind: function (id) {
        for (let i = 0; i < audio.list.length; i++) {
            if (audio.list[i].id == id) return audio.list[i].kind;
        }
    },
    get: function(id) {
        for (let i = 0; i < audio.list.length; i++) {
            if (audio.list[i].id == id) return audio.list[i].audio;
        }
    },
    fotime: function(id, val) {
        for (let i = 0; i < audio.list.length; i++) {
            if (val == undefined) {
                if (audio.list[i].id == id) return audio.list[i].fotime;
            } else {
                if (audio.list[i].id == id) return audio.list[i].fotime = val;
            }
        }
    },
    error: function(id) {
        for (let i = 0; i < audio.list.length; i++) {
            if (audio.list[i].id == id) return audio.list[i].error;
        }
    }
};

let scenes = {
    list: [],
    load: function (data) {
        let regex = /\\subsection\{([^\}]*)\}\s*\\input\{scenes\/([A-Za-z0-9]+)\.tex\}/g;

        let match = regex.exec(data);

        while (match != null) {
            let scene = {
                id: match[2],
                title: match[1],
                content: ""
            }

            scene.load = function (data) {
                data = data.replace(/%\?begin[^$]*?%\?end/g, "")
                    .replace(/\$\$[^$]+\$\$/g, "")
                    .replace(/\$[^$]+\$/g, "")
                    .replace(/``([^$]*?)''/g, '&ldquo;<i>$1</i>&rdquo;')
                    .replace(/\\emph\{([^\}]*?)\}/g, "<i>$1</i>")
                    .replace(/\{\\bf ([^\}]*?)\}/g, "<b>$1</b>")
                    .replace(/\\begin\{itemize\}\s*([\W\w]*?)\\end\{itemize\}/g, (match, content) => {
                        return "<ul>" + content.replace(/\\item\s*(.*)(?!\\item)/g, "<li>$1</li>") + "</ul>";
                    }).replace(/\\Call\{([^\}]*)\}/g, (match, id) => {
                        return characters.call(id);
                    }).replace(/\\Name\{([^\}]*)\}/g, (match, id) => {
                        return characters.name(id);
                    }).replace(/\\NameAlt\{([^\}]*)\}/g, (match, id) => {
                        return characters.name(id, 1);
                    }).replace(/\\NameAltA\{([^\}]*)\}/g, (match, id) => {
                        return characters.name(id, 2);
                    }).replace(/\\NameAltB\{([^\}]*)\}/g, (match, id) => {
                        return characters.name(id, 3);
                    }).replace(/\\NameAltC\{([^\}]*)\}/g, (match, id) => {
                        return characters.name(id, 4);
                    }).replace(/\\NameAltD\{([^\}]*)\}/g, (match, id) => {
                        return characters.name(id, 5);
                    }).replace(/\\Use\{([^\}]*)\}/g, (match, id) => {
                        return id; // PROVVISORIO
                    })
                    .replace(/\\rem\{([^\}]*)\}/g, "<span class=\"rem\">$1</span>")
                    .replace(/\\act\{([^\}]*)\}/g, "<span class=\"act\">$1</span>")
                    .replace(/(\\\\|\\newline)/g, "<br />")
                    .replace(/\\Speech\{([^\}]*)\}\{([^\}]*)\}/g, (match, id, content) => {
                        return "<div class=\"speech\">"
                            + "<div class=\"character\">" + characters.call(id) + "</div>"
                            + "<div class=\"text\">" + content + "</div>"
                            +"</div>";
                    }).replace(/\\ExtSpeech\{([^\}]*)\}\{([^\}]*)\}/g, (match, text, content) => {
                        return "<div class=\"speech\">"
                            + "<div class=\"character-ext\">" + text + "</div>"
                            + "<div class=\"text\">" + content + "</div>"
                            +"</div>";
                    }).replace(/\\Act\{([^\}]*)\}/g, (match, content) => {
                        return "<div class=\"act\">" + content + "</div>";
                    }).replace(/\\Rem\{([^\}]*)\}/g, (match, content) => {
                        return "<div class=\"rem\">" + content + "</div>";
                    }).replace(/\\Play\{([^\}]*)\}/g, (match, id) => {
                        if (audio.kind(id) == 'playback' || audio.kind(id) == 'sound') {
                            console.log(id, audio.error(id));
                            return "<div class=\"audio\">"
                                + "<button onclick=\"stopbtn_click('" + id + "')\"><img src=\"./res/stop.png\" /></button>"
                                + "<button onclick=\"pausebtn_click('" + id + "')\"><img src=\"./res/pause.png\" /></button>"
                                + "<button onclick=\"playbtn_click('" + id + "')\"><img src=\"./res/play.png\" /></button>"
                                + "<input type=\"number\" oninput=\"fonum_scroll('" + id + "', event);\" min=\"0\" max=\"5.0\" step=\"0.1\" value=\"1.0\" /></input>"
                                + "<button onclick=\"fadeoutbtn_click('" + id + "')\"><img src=\"./res/fadeout.png\" /></button>"
                                + "<input type=\"range\" oninput=\"volume_scroll('" + id + "', event);\" min=\"0\" max=\"100\" value=\"100\" class=\"volume\" /></input>"
                                + "<input id=\"audio-" + id + "-volume\" type=\"range\" oninput=\"time_scroll('" + id + "', event);\" min=\"0\" max=\"" + audio.get(id).duration + "\" step=\"0.1\" value=\"0\" class=\"time\" /></input>"
                                + (audio.error(id) ? "<img src=\"./res/error.png\" title=\"Error loading audio file\"/> " : "")
                                + "<h4>" + audio.title(id) + "</h4>"
                                + "</div>";
                        } else {
                            return "<div class=\"audio\">"
                                + "<h4>" + audio.title(id) + "</h4>"
                                + "</div>";
                        }
                    });
                this.content = data;
            }.bind(scene)

            scenes.list.push(scene);

            match = regex.exec(data);
        }

        let content = "";

        scenes.list.forEach((val, i) => {
            $("#loading-bar-slave").attr("name", "scenes/" + val.id + ".tex");
            set_progress("loading-bar-master", Math.trunc(20 + i * 80 / scenes.list.length));
            $.ajax("./scenes/" + val.id + ".tex", {
                async: false,
                cache: false,
                success: val.load
            });

            content += "<h2 id=\"" + val.id + "\">" + val.title + "</h2>\n" + val.content;

            $('#menu-content').append("<a href=\"#" + val.id + "\">" + val.title + "</a>");

            set_progress("loading-bar-master", Math.trunc(20 + (i + 1) * 80 / scenes.list.length));
        });

        $("#body").html(content);
    }
}

function playbtn_click(id) {
    audio.get(id).play();

    audio.current_id = id;
}

function pausebtn_click(id) {
    audio.get(id).pause();

    audio.current_id = id;
}

function stopbtn_click(id) {
    let a = audio.get(id);

    a.pause();
    a.currentTime = 0;

    audio.current_id = id;
}

function volume_scroll(id, e) {
    audio.get(id).volume = e.target.value / 100.0;
}

function fonum_scroll(id, e) {
    audio.fotime(id, e.target.value);
}

function fadeoutbtn_click(id) {
    let a = audio.get(id);
    let v = a.volume;
    let rate = v / (audio.fotime(id) * 100.0);

    let lowerer_int = {value: null};
    
    let lowerer = () => {
        if (a.volume > rate) {
            a.volume -= rate;
        } else {
            clearInterval(lowerer_int.value);
            a.pause();
            a.volume = v;
        }
    };

    lowerer_int.value = setInterval(lowerer, 10);
}

function set_progress(id, value) {
    $("#" + id + " > span").css("width", value + "%");
}

$(() => {
    // Initialize loading screen.
    $(".progress-bar").html("<span></span>");
    set_progress("loading-bar-master", 0);
    set_progress("loading-bar-slave", 0);

    // Show loading screen.
    $("#loading-screen").fadeIn(200);
    setTimeout(function(){}, 10);

    // Load characters.
    $("#loading-bar-master").attr("name", "partial/characters.tex");
    $.ajax("./partial/characters.tex", {
        async: false,
        cache: false,
        success: characters.load
    });
    setTimeout(function(){}, 10);

    // Load audio files.
    $("#loading-bar-master").attr("name", "partial/audio.tex");
    $.ajax("./partial/audio.tex", {
        async: false,
        cache: false,
        success: audio.load
    });
    setTimeout(function(){}, 10);

    // Add scenes menu section.
    $("#menu-content").append("<h2>Scene</h2>");
    
    // Load scenes.
    set_progress("loading-bar-master", 12);
    $("#loading-bar-master").attr("name", "scenes...");
    $.ajax("./master.tex", {
        async: false,
        cache: false,
        success: scenes.load
    });

    // Add utility menu section.
    $("#menu-content").append("<h2>Utilit&agrave;</h2>");
    $("#menu-content").append("<a href=\"./screen.html\" target=\"_blank\">Apri schermo</a>");
    $("#menu-content").append("<a href=\"#\" onclick=\"screen_tools.show();\">Strumenti schermo</a>");

    sw.install();

    $(document).keypress((e) => {
        if (e.target.nodeName == "TEXTAREA") return;

        e.preventDefault();

        switch (e.key) {
            case '3':
                playbtn_click(audio.current_id);
                break;
            case '2':
                pausebtn_click(audio.current_id);
                break;
            case '1':
                stopbtn_click(audio.current_id);
                break;
            case '4':
                fadeoutbtn_click(audio.current_id);
                break;
            case '-':
                screen_tools.lyrics.prev();
                break;
            case '+':
                screen_tools.lyrics.next();
                break;
        }

        console.log(e);

        console.log(e.key);
    });

    // End!
    $("#loading-screen").fadeOut(200);
});