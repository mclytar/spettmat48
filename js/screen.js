let sw = {
    install: function() {
        navigator.serviceWorker.register('./sw.js').then((registration) => {
            console.log("ServiceWorker registered correctly!");

            registration.update();

            navigator.serviceWorker.addEventListener('message', event => {
                if (event.data.target == 'screen') sw.onmessage(event.data);
            });
        });
    },
    broadcast: function(message) {
        return new Promise((resolve, reject) => {
            let msg_ch = new MessageChannel();
    
            msg_ch.port1.onmessage = event => {
                if (event.data.error) reject(event.data.error);
                else resolve(event.data);
            }

            navigator.serviceWorker.controller.postMessage(message, [msg_ch.port2]);
        });
    },
    onmessage: data => {
        console.log(data);

        switch (event.data.action) {
            case 'text-set':
                $('#content').removeClass('song');
                $('#content').text(event.data.content);
                break;
            case 'html-set':
                $('#content').removeClass('song');
                $('#content').html(event.data.content);
                break;
            case 'lyric-set':
                $('#content').addClass('song');
                $('#content').text(event.data.content);
                if (event.data.next) {
                    $('#content').append("<br /><span class='next'>" + event.data.next + "</span>");
                }
                break;
            case 'clear':
                $('#content').text("");
            default:
                break;
        }
    }
};

$(() => {
    sw.install();
});