# Partecipanti

Questo è un elenco di tutti coloro che hanno reso possibile la realizzazione di questo spettacolo.

## Regia

- Dario Fiorenza

## Aiuto Regia

- Alessandro Lehmann
- Gianluca Brian
- Riccardo Fasano

## Gestione informatica e audio/video

- Gianluca Brian

## Organizzazione

- Irene Solaini
- Nicoletta Capotorto

### Servizio d'ordine

- Angelo Amoriello
- Edoardo Fibbi
- Elio Campanile
- Giulia Scaccia
- Iacopo Giordano
- Ivano Di Gioia
- Martin Kolodziejczyk
- Nathan Clay
- Simone Guidotti

### Raccolta fondi

- Benedetta Ferrera
- Claudia Capuzzi
- Debora Spaziani
- Francesco Brugnami
- Simone Guidotti

## Scrittura

- Prof. Annalisa Malusa
- Prof. Ivano Salvo
- Prof. Luca Fanelli
- Prof. Luigi Orsina
- Alessandro Lehmann
- Alessandro Vannini
- Andrea Drago
- Andrea Monaco
- Dario Fiorenza
- Elio Campanile
- Francesco Capomassi
- Francesco Paletta
- Gianluca Brian
- Riccardo Fasano
- Roberto Fratello
- Rocco Brunelli

## Attori

- Prof. Annalisa Malusa (**Mary Darling**)
- Prof. Eugenio Montefusco (**CoccoFusco**, **Monte Fusco**)
- Prof. Ivano Salvo (**AgenHoare**)
- Lorenzo De Pasqua (**De Pasqua**)
- Prof. Luca Fanelli (**Poggio delle Ginestre**)
- Prof. Luigi Orsina (**Vento Nei Capelli**)
- Prof. Vincenzo Nesi (**Baffo Spettinato**)
- Agnese Janigro (**Fermione Lagrange**, **Indiano 6**, **Zermelope**)
- Alessandro Blasetti (**Dante**)
- Alessandro Vannini (**Bimbo Dottorando**, **Haargrid**)
- Andrea Drago (**Drago Malfoy/Renzorn**, **Ombra**)
- Andrea Mazzocco (**Hardy Potter**, **Indiano 2**)
- Beatrice Marchitelli (**Regolo**, **Segreteria Didattica**)
- Benedetta Ferrera (**Vendy**)
- Carlotta Petrucci (**Spettro del Natale Passato**)
- Chiara Graziani (**Lusina**)
- Chiara Plati (**Bimba liceale**)
- Daniele Avino (**Indiano 1**, **Simbolo**)
- Davide Donati (**Banach**)
- Debora Spaziani (**Giglio Fibrato**, **Macchinetta del Caffè**, **Segreteria Amministrativa**)
- Diana Bortot (**Francesca**)
- Donatella Genovese (**Bimba Fuoricorso**, **Oste**)
- Eleonora Di Carluccio (**Pikard**)
- Eleonora Longoni (**Pirata Ballerina**)
- Fabrizio Calimera (**Giullare 2008**)
- Fausto Colantoni (**Sirena Transfinita 1**, **Spugna**)
- Filippo Carmine Comparato (**Peter Span**)
- Flavio Mattiuzzo (**Indiano 3**, **Ron Whitney**)
- Francesco Brugnami (**Tarski**)
- Francesco Cianfrocca (**Bimbo Erasmus**)
- Francesco Paletta (**Lemma Serpente**, **Pirata Fisarmonico**, **Procio Letterato**)
- Giacomo Sorgente (**Angolo**, **Giullare 2009**)
- Giammarco Remedi (**Pendolo**)
- Guendalina Crescenzi (**Eulero**)
- Ivo Simeoni (**Lemma Serpente**, **Pirata Chimico**, **Procio Chimico**)
- Lorenzo De Biase (**C Cappuccetto**)
- Luca Campagna (**Capitan Hooke**, **Sirena Transfinita 3**)
- Luigi Savino (**Lemma Serpente**, **Pirata Informatico**, **Procio Informatico**)
- Maria De Filippo (**Spettro del Natale Presente**)
- Martina De Marchis (**Burocrazia**, **Godellum**, **Spettro del Natale Futuro**)
- Matteo Di Nitto (**Luppolo**, **Sirena Transfinita 2**)
- Michele Ferrante (**Darth Rodrigo**, **Maria De Filippi**)
- Nicolò Geracitano (**Spigolo**, **De Morgan**)
- Nicolò Paracino (**Lemma Serpente**, **Pirata Fisico**, **Procio Fisico**)
- Rachele Esposito (**Trill**)
- Riccardo Fasano (**Bimbo Banale**)
- Roberto Fratello (**L'Innominabile**, **Nana**, **Pinocchio**)
- Roberto Taberini (**Paolo**)
- Serena Boccia (**Bibliotecaria**, **Ippografo**)
- Simone Castellan (**Calcolo**, **Indiano 4**, **Virgilio**)
- Vincenzo Silvestri (**Giullare 2010**, **Indiano 5**, **Otto**)
- Yarema Gapyak (**Corango Tango**, **Medico**)

## Operatori di scena

- Carlo Farneti
- Ginevra Aquilina

## Musica

### Autori

- Prof. Luca Fanelli(**6x7+2 Gatti**)
- Prof. Luigi Orsina(**6x7+2 Gatti**)
- Alessandro Lehmann (**Algebra**, **Titanic**)
- Andrea Drago (**Titanic**)
- Eleonora Di Carluccio (**Algebra**)
- Fausto Colantoni (**Rap, parte 1**)
- Filippo Carmine Comparato (**Algebra**)
- Luca Campagna(**Rap, parte 2**)
- Riccardo Fasano (**Titanic**)
- Roberto Fratello (**Aria di Eulero-Vendy**, **E**, **Gloria al Funtore**, **Il Limite che non c'è**)

### Musicisti

- Prof. Luca Fanelli (_tastiera_, **6x7+2 Gatti**)
- Beatrice Marchitelli (_violoncello_, **Aria di Eulero-Vendy**, **Rap, parte 1**, **Rap, parte 2**)
- Francesca Sanzò (_chitarra elettrica_, **Algebra**, **E**)
- Gabriele Capuano (_beatbox_, _tastiera_, **Aria di Eulero-Vendy**, **E**, **Rap, parte 1**, **Rap, parte 2**)
- Giammarco Remedi (_chitarra acustica_, _chitarra elettrica_, **Algebra**, **E**, **Il Limite che non c'è**, **Rap, parte 1**, **Rap, parte 2**)
- Ivan Ferrara (_bongo_, **Algebra**)
- Matteo Di Nitto (_tastiera_, **Rap, parte 1**, **Rap, parte 2**)
- Simone Bonaldo (_basso elettrico_, **Algebra**, **E**, **Rap, parte 1**, **Rap, parte 2**)

### Cantanti

- Prof. Luca Fanelli (**6x7+2 Gatti**)
- Prof. Luigi Orsina (**6x7+2 Gatti**)
- Eleonora Di Carluccio (**Algebra**)
- Fausto Colantoni (**Rap, parte 1**)
- Filippo Carmine Comparato (**Algebra**, **Gloria al Funtore**, **Il limite che non c'è**)
- Guendalina Crescenzi (**Algebra**, **Aria di Eulero-Vendy**, **Il limite che non c'è**, **Titanic**)
- Luca Campagna (**Rap, parte 2**)
- Roberto Fratello (**E**)

## Scenografia e costumi

- Beatrice Marchitelli
- Benedetta Ferrera
- Debora Spaziani

## Aiuto scenografia e costumi

- Andrea Drago
- Giulia Latagliata
- Guendalina Crescenzi